#ifndef MYWINDOW_HPP
#define MYWINDOW_HPP

namespace myname {


class MyWindow
{
protected:
    int left;
    int top;
    int width;
    int height;
    char *buf;
    char *title;
    void initWindow(const int &x, const int &y, const int &w, const int &h);
public:
    MyWindow();
    MyWindow(const int &x, const int &y, const int &w, const int &h, const char *t);
    ~MyWindow();
    virtual void show() const;
    const char * setTitle(const char *t);
};






}

//end of mywindow.hpp
#endif
