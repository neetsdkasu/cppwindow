#include <cstdlib>
#include <cstring>
#include <conio.h>
#include "mywindow.hpp"

using namespace std;

namespace myname {

    //Constructor
    MyWindow::MyWindow() {
        initWindow(1, 1, 20, 7);
        setTitle(NULL);
    }
    
    //Constructor
    MyWindow::MyWindow(const int &x,const int &y,const int &w,const int &h, const char *t) {
        initWindow(x, y, w, h);
        setTitle(t);
    }
    
    //Destructor
    MyWindow::~MyWindow() {
        delete [] buf;
        delete [] title;
    }
    
    //表示
    void MyWindow::show() const {
        int i, h = top + height;
        char *p;
        
        textattr(LIGHTGRAY << 4 | BLUE);
        gotoxy(left, top);
        cputs(buf);
        
        p = buf + (width + 1);
        textattr(BLUE << 4 | LIGHTGRAY);
        for (i = top + 1; i < h; i++) {
            gotoxy(left, i);
            cputs(p);
        }
        
        p += width + 1;
        gotoxy(left, i);
        cputs(p);
        
    }
    
    //ウィンドウの初期化
    void MyWindow::initWindow(const int &x, const int &y, const int &w, const int &h) {
        char *p, *q;
        int i, e;
        
        left   = x;
        top    = y;
        width  = w;
        height = h;
        title  = NULL;
        buf    = new char[(width + 1) * 3];
        
        p = buf + (width + 1);
        q = p + (width + 1);
        e = width - 1;
        *p++ = *q++ = '|';
        for (i = 1; i < e; i++) {
            *p++ = ' ';
            *q++ = '-';
        }
        *p++ = *q++ = '|';
        *p = *q = '\0';
        buf[width] = '\0';
    }
    
    //タイトルの設定
    const char * MyWindow::setTitle(const char * const t) {
        int len, pos, i;
        
        delete [] title;
        
        for (i = 0; i < width; i++) {
            buf[i] = ' ';
        }
        
        if (t == NULL) {
            return title = NULL;
        }
        
        len = strlen(t);
        title = new char[len + 1];
        strcpy(title, t);
        
        pos = (width - len) / 2;
        if (pos < 0) {
            pos = 0;
        }
        if (width < len) {
            len  = width;
        }
        strncpy(buf + pos, title, len);
        
        return title;
    }


} //namespace myname

