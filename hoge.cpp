#include <iostream>
#include <conio.h>
#include "mywindow.hpp"


int main() {
    
    using namespace std;
    using namespace myname;
    
    MyWindow win;
    MyWindow win1(3, 3, 20, 6, "MyTitle1");
    MyWindow win2(8, 5, 20, 6, "MyTitle2");
    
    clrscr();
    win.show();
    getch();
    win1.show();
    getch();
    win2.show();
    getch();
    
    win1.setTitle("12345678901234567890123");
    win1.show();
    getch();
    
    cout << endl << endl << "hoge" << endl;
    
    return 0;
}
